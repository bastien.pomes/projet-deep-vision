import os
from tqdm import tqdm
import PIL.Image as Image

import torch
from torch.autograd import Variable
from torchvision import transforms

from model import Net

MODEL_PATH = 'model/model_15.pth'
OUTPUT_PATH = 'result/pred.csv'

state_dict = torch.load(MODEL_PATH)
model = Net()
model.load_state_dict(state_dict)
model.eval()

test_dir = '../GTSRB/test_images/Images'

def pil_loader(path):
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')

if __name__ == '__main__':

    transform = transforms.Compose([
            transforms.Resize((32, 32)),
            transforms.ToTensor(),
            transforms.Normalize((0.3337, 0.3064, 0.3171), ( 0.2672, 0.2564, 0.2629))
        ])
    output_file = open(OUTPUT_PATH, "w")
    output_file.write("Filename,ClassId\n")

    for f in tqdm(os.listdir(test_dir)):
        if 'ppm' in f:
            output = torch.zeros([1, 43], dtype=torch.float32)
            with torch.no_grad():
                data = transform(pil_loader(test_dir + '/' + f))
                data = data.view(1, data.size(0), data.size(1), data.size(2))
                data = Variable(data)
                output = output.add(model(data))
                pred = output.data.max(1, keepdim=True)[1]
                file_id = f[0:5]
                output_file.write("%s,%d\n" % (file_id, pred))

    output_file.close()


