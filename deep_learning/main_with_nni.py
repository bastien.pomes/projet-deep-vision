import torch
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import numpy as np
from model import Net
import nni

# Training settings

RECEIVED_PARAMS = nni.get_next_parameter()

BATCH_SIZE = 64
EPOCHS = 15
LEARNING_RATE = RECEIVED_PARAMS['learning_rate']

if __name__ == '__main__':

    if torch.cuda.is_available():
        use_gpu = True
        print("Using GPU")
    else:
        use_gpu = False
        print("Using CPU")

    FloatTensor = torch.cuda.FloatTensor if use_gpu else torch.FloatTensor
    LongTensor = torch.cuda.LongTensor if use_gpu else torch.LongTensor
    ByteTensor = torch.cuda.ByteTensor if use_gpu else torch.ByteTensor
    Tensor = FloatTensor

    data_transforms = transforms.Compose([
        transforms.Resize((32, 32)),
        transforms.ToTensor(),
        transforms.Normalize((0.3337, 0.3064, 0.3171), ( 0.2672, 0.2564, 0.2629))
    ])

    train_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder('../GTSRB/train_images/Images',
                            transform=data_transforms),
        batch_size=BATCH_SIZE, shuffle=True, num_workers=4, pin_memory=use_gpu)
    
    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder('../GTSRB/val_images/Images',
                            transform=data_transforms),
        batch_size=BATCH_SIZE, shuffle=True, num_workers=4, pin_memory=use_gpu)
    

    model = Net()

    if use_gpu:
        model.cuda()

    optimizer = optim.Adam(filter(lambda p: p.requires_grad,model.parameters()),lr=LEARNING_RATE)

    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',patience=5,factor=0.5,verbose=True)

    def train(epoch):
        model.train()
        correct = 0
        training_loss = 0
        for batch_idx, (data, target) in enumerate(train_loader):
            data, target = Variable(data), Variable(target)
            if use_gpu:
                data = data.cuda()
                target = target.cuda()
            optimizer.zero_grad()
            output = model(data)
            loss = F.nll_loss(output, target)
            loss.backward()
            optimizer.step()
            max_index = output.max(dim = 1)[1]
            correct += (max_index == target).sum()
            training_loss += loss
            if batch_idx % 100 == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss per example: {:.6f}\tLoss: {:.6f}'.format(
                            epoch, batch_idx * len(data), len(train_loader.dataset),
                            100. * batch_idx / len(train_loader), loss.data.item()/(BATCH_SIZE * 100),loss.data.item()))
        print('\nTraining set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
                    training_loss / len(train_loader.dataset), correct, len(train_loader.dataset),
                    100. * correct / len(train_loader.dataset)))

    def validation():
        model.eval()
        validation_loss = 0
        correct = 0
        for data, target in val_loader:
            with torch.no_grad():
                data, target = Variable(data), Variable(target)
                if use_gpu:
                    data = data.cuda()
                    target = target.cuda()
                output = model(data)
                validation_loss += F.nll_loss(output, target, size_average=False).data.item() # sum up batch loss
                pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
                correct += pred.eq(target.data.view_as(pred)).cpu().sum()

        validation_loss /= len(val_loader.dataset)
        scheduler.step(np.around(validation_loss,2))

        accuracy = int(100. * correct / len(val_loader.dataset))

        print('\nValidation set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
            validation_loss, correct, len(val_loader.dataset), accuracy))

        return accuracy

    for epoch in range(1, EPOCHS + 1):
        train(epoch)
        accuracy = validation()
        model_file = 'model/model_' + str(epoch) + '.pth'
        torch.save(model.state_dict(), model_file)
        print('\nSaved model to ' + model_file)
        nni.report_intermediate_result(accuracy)
    nni.report_final_result(accuracy)

