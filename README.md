# Projet Deep Vision

L'ensemble des scripts présents dans ce dossier permettent de lancer et de comprendre les algorithmes présentés dans le rapport du projet "Reconnaissance visuelle de panneaux de signalisation routière".

Les jeux de données ne sont pas présents dans ce git mais peuvent être téléchargés à l'adresse suivante:
https://benchmark.ini.rub.de/gtsrb_dataset.html


## Explication du code

Pour exécuter le code, il faudra au préalable avoir téléchargé le dataset et le mettre dans un dossier GTSRB séparé en trois : 
train_images, val_images et test_images.

Le code est séparé en deux partie: une partie computer vision et une partie deep learning.

### Computer Vision
Pour lancer la détection de contours ou de cercles sur une image aléatoire du dataset de test, exécuter la commande:

```python detect_contours.py```

ou

```python detect_hough.py```

### Deep Learning
Pour lancer l'entraînement du modèle sur le dataset, exécuter :

```python main.py```

Les modèles seront sauvegardés par epoch dans le dossier model et le score sur le dataset val sera affiché.

Pour tester l'influence du learning rate, télécharger nni `(pip install nni)` puis exécuter :

```nnictl create --config search_space.yml```

Vous aurez alors un lien vers un localhost pour visualiser les graphiques et l'avancement de l'entraînement.

Une fois votre modèle entraîné, exécuter :

```python evaluate.py```

Vous aurez alors un fichier csv de prédiction du dataset de test.

Pour obtenir les métriques, la matrice de confusion et le rapport de classification, utiliser le notebook `compare_result.ipynb` dans le dossier result.


## Auteur

Bastien Pomes
