
"https://www.kaggle.com/datasets/andrewmvd/road-sign-detection"
import cv2
import os, random

dir_path = '../GTSRB/test_images/Images'
imgpath = os.path.join(dir_path,random.choice(os.listdir(dir_path)))

image = cv2.imread(imgpath)

#Affichage de l'image d'origine
im = cv2.resize(image, (300, 300)) 
cv2.imshow('im',im)
cv2.waitKey(0)

#Fonction qui vérifie si un contour détecté vérifie quelques propriétés
def verifContours(im,cnt):

    #On regarde si le contour n'est pas trop petit ou trop grand
    if len(cnt) < 100 or len(cnt) > 2000:
        return (False,None)

    #On cherche le rectangle qui entoure ce contour
    perimetre=cv2.arcLength(cnt,True)
    approx = cv2.approxPolyDP(cnt,0.01*perimetre,True)
    (x, y, w, h) = cv2.boundingRect(approx)

    #On vérifie que la forme est proche d'un carré
    if w > 1.5*h or h > 1.5*w:
        return (False,None)

    print(len(approx))

    return (True, cnt)

#fonction qui renvoie les contours des logos sur une image
def findlogo(im):

    #on applique d'abord un traitement pour trouver les changement de gradient de couleur sur l'image
    img = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(img,100,200)

    #on cherche ensuite les contours sur cette image
    ret, thresh = cv2.threshold(edges, 127, 255, 0)
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

    finalcontour = []

    #pour chaque contour, on regarde si il est acceptable
    for cnt in contours:
        verif = verifContours(im,cnt)
        if verif[0]:
            finalcontour.append(verif[1])

    return finalcontour

contours = findlogo(image)

if contours:
    # On trace ici chaque contour et l'ajoute à la liste de contour
    for cnt in contours:
        cv2.drawContours(image,cnt,-1, (0,255,0), 1)

image = cv2.resize(image, (300, 300)) 
cv2.imshow('im',image)
cv2.waitKey(0)