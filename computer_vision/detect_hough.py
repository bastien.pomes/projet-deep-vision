import cv2 
import numpy as np 
import os
import random

dir_path = '../GTSRB/test_images/Images'
imgpath = os.path.join(dir_path,random.choice(os.listdir(dir_path)))

img = cv2.imread(imgpath,cv2.IMREAD_COLOR)

#Affichage de l'image d'origine
im = cv2.resize(img, (300, 300)) 
cv2.imshow('im',im)
cv2.waitKey(0)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img_blur = cv2.medianBlur(gray, 5)
# Application de la transformée de Hough
circles = cv2.HoughCircles(img_blur, cv2.HOUGH_GRADIENT, 1, img.shape[0]/64, param1=200, param2=10, minRadius=5, maxRadius=30)
# Affichage des cercles sur l'image
if circles is not None:
    circles = np.uint16(np.around(circles))
    best_circle = circles[0, :][0]
    cv2.circle(img, (best_circle[0], best_circle[1]), best_circle[2], (0, 255, 0), 2)

image = cv2.resize(img, (300, 300)) 
cv2.imshow('im',image)
cv2.waitKey(0)